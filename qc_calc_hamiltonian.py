""" 
# qc_calc_hamiltonian

is a module which provides a small set of functions to
perform some calculations on a system of n qubits, described by a specific
hamiltonian model, such that:

$$
\hat{H} = -J \sum_{j=1}^{N-1} \left( \hat{\sigma}_{j}^{x}
\hat{\sigma}_{j+1}^{x} + \hat{\sigma}_{j}^{y}
\hat{\sigma}_{j+1}^{y} \right) + U \sum_{j=1}^{N-1} \hat{\sigma}_{j}^{z}
\hat{\sigma}_{j+1}^{z} + \sum_{j=1}^{N} h_{j} \hat{\sigma}_{j}^{z}
$$

In particular, the module calculates:

- The tensor product resulting of applying a set of gates to a corresponding set
of qubits
- The hamiltonian
- The adjoint hamiltonian (just to verify that it is, in fact, hermitian)
- The eigenvalues and eigenvectors of the hamiltonian

It also defines some functions to print out into log files each one of those
elements of the simulation.

Some functions may come if/when needed.
"""
import logging

import numpy as np
import numpy.typing as npt
from scipy.linalg import expm


def _set_logger_config() -> logging.Logger:
    new_logger: logging.Logger = logging.getLogger(__name__)

    formatter: logging.Formatter = logging.Formatter(
        "{asctime}->{levelname}->{name}->{funcName}->{lineno}:\n{message}",
        style="{",
    )

    file_handler: logging.FileHandler = logging.FileHandler(
        "qc_calc_hamiltonian.log", mode="w", encoding="utf-8"
    )

    file_handler.setFormatter(formatter)

    new_logger.addHandler(file_handler)

    new_logger.setLevel(logging.INFO)

    return new_logger


_logger = _set_logger_config()

J: float = 1
U: float = 1
h: float = 1

ident: npt.NDArray = np.identity(2)
sigma_x: npt.NDArray = np.asarray([[0, 1], [1, 0]])
sigma_y: npt.NDArray = np.asarray([[0, -1j], [1, 0]])
sigma_z: npt.NDArray = np.asarray([[1, 0], [0, -1]])


def tensor_product(
    n: int, *operators: tuple[npt.ArrayLike, int]
) -> npt.NDArray:
    """tensor_product returns the tensor product corresponding to n consecutive
    quantum gates applied to n qubits.

    Parameters
    ----------
    n : int
        The number of qubits on the system.

    operators : tuple[ArrayLike, int]
        A tuple (or a collection of them) with a quantum gate of one qubit
        as the first argument and the index of the qubit in the system which it
        is applied to.

    Returns
    -------
        resulting_gate: NDArray
            Resulting quantum gate of dimension $2^n$
    """

    assert operators
    resulting_gate: npt.NDArray = np.array([])
    indexes: list[int] = []

    # _logger.info(operators)

    for op in operators:
        indexes.append(op[1])

    indexes.sort()
    ordered_ops: list[tuple[npt.ArrayLike, int]] = []

    for i in indexes:
        for op in operators:
            if op[1] == i:
                ordered_ops.append(op)

    resulting_gate = ident
    k: int = len(indexes)

    if n in indexes:
        resulting_gate = np.array(ordered_ops[-1][0])
        k: int = len(indexes) - 1

    for j in range(n - 1, 0, -1):
        if j in indexes:
            resulting_gate = np.kron(ordered_ops[k - 1][0], resulting_gate)

            k -= 1
        else:
            resulting_gate = np.kron(ident, resulting_gate)

    return resulting_gate


def calc_first_term(n: int, J: float) -> npt.NDArray:
    term_1: npt.NDArray = np.zeros((2**n, 2**n))
    # print(f"Step 1: \n {term_1} \n")

    for j in range(1, n):
        a: npt.NDArray = tensor_product(n, (sigma_x, j), (sigma_x, j + 1))
        b: npt.NDArray = tensor_product(n, (sigma_y, j), (sigma_y, j + 1))

        # print(f"\n a is: \n {a} \n")
        # print(f"\n b is: \n {b} \n")

        term_1 = (
            term_1
            + tensor_product(n, (sigma_x, j), (sigma_x, j + 1))
            + tensor_product(n, (sigma_y, j), (sigma_y, j + 1))
        )

        # print(f"j = {j} \n Step 2: \n {term_1} \n")

    return -J * term_1


def calc_second_term(n: int, U: float) -> npt.NDArray:
    term_2: npt.NDArray = np.zeros((2**n, 2**n))
    # print(f"Step 1: \n {term_2} \n")

    for j in range(1, n):
        a: npt.NDArray = tensor_product(n, (sigma_z, j), (sigma_z, j + 1))

        # print(f"\n a es: \n {a} \n")

        term_2 += tensor_product(n, (sigma_z, j), (sigma_z, j + 1))

        # print(f"j = {j} \n Step 2: \n {term_2} \n")

    return U * term_2


def calc_third_term(n: int, h: float) -> npt.NDArray:
    term_3: npt.NDArray = np.zeros((2**n, 2**n))
    # print(f"Step 1: \n {term_3} \n")

    for j in range(1, n + 1):
        a: npt.NDArray = tensor_product(n, (sigma_z, j))

        # print(f"\n a es: \n {a} \n")

        term_3 += tensor_product(n, (sigma_z, j))

        # print(f"j = {j} \n Step 2: \n {term_3} \n")

    return h * term_3


def calc_hamiltonian(n: int, J: float, U: float, h: float) -> npt.NDArray:
    term1: npt.NDArray = calc_first_term(n, J)

    term2: npt.NDArray = calc_second_term(n, U)

    term3: npt.NDArray = calc_third_term(n, h)

    terms: list[npt.NDArray] = [term1, term2, term3]

    hamiltonian: npt.NDArray = np.zeros((2**n, 2**n))

    for term in terms:
        hamiltonian = hamiltonian + term

    return hamiltonian


def print_hamiltonian(hamiltonian: npt.ArrayLike) -> None:
    _logger.info(f"The hamiltonian is: \n {hamiltonian} \n")


def calc_adjoint(obj: npt.NDArray) -> npt.NDArray:
    adjoint_obj = np.transpose(np.conjugate(obj))

    return adjoint_obj


def calc_adjoint_hamiltonian(hamiltonian: npt.NDArray) -> npt.NDArray:
    return calc_adjoint(hamiltonian)


def print_adjoint_hamiltonian(hamiltonian: npt.NDArray) -> None:
    _logger.info(
        "The adjoint of the hamiltonian is: \n"
        f" {calc_adjoint_hamiltonian(calc_adjoint(hamiltonian))} \n"
    )


def calc_eigen(hamiltonian: npt.NDArray) -> tuple[npt.NDArray, npt.ArrayLike]:
    eig_val: npt.NDArray
    eig_vec: npt.ArrayLike
    eig_val, eig_vec = np.linalg.eig(hamiltonian)

    return (eig_val, eig_vec)


def print_eigen(eig_val: npt.NDArray, eig_vec: npt.ArrayLike) -> None:
    _logger.info(
        f"The eigenvalues and normalized eigenvectors of the hamiltonian are:"
    )

    for i, (val, vec) in enumerate(zip(eig_val, eig_vec), start=1):
        i: int
        val: complex
        vec: npt.ArrayLike
        _logger.info(
            f"{i} -> eigen value: \n {val} \n  -> eigen vector: \n {vec} \n\n"
        )


def normalize_vector(v: npt.ArrayLike) -> npt.NDArray:
    v = np.array(v)
    assert np.linalg.norm(v) != 0
    return v / np.linalg.norm(v)


def calc_expectation_value(
    n: int, operator: npt.ArrayLike, ket: npt.ArrayLike
) -> complex:
    ket = np.array(ket)
    assert ket.shape == (2**n,)
    bra: npt.NDArray = np.transpose(np.conjugate(ket))
    op_dot_ket: npt.NDArray = operator.dot(ket)

    return bra.dot(op_dot_ket)
