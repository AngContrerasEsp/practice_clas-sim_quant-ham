# qc_calc_hamiltonian

is a module which provides a small set of functions to
perform some calculations on a system of n qubits, described by a specific
hamiltonian model, such that:

```math
\hat{H} = -J \sum_{j=1}^{N-1} \left( \hat{\sigma}_{j}^{x}
\hat{\sigma}_{j+1}^{x} + \hat{\sigma}_{j}^{y}
\hat{\sigma}_{j+1}^{y} \right) + U \sum_{j=1}^{N-1} \hat{\sigma}_{j}^{z}
\hat{\sigma}_{j+1}^{z} + \sum_{j=1}^{N} h_{j} \hat{\sigma}_{j}^{z}
```

In particular, the module calculates:

- The tensor product resulting of applying a set of gates to a corresponding set
of qubits
- The hamiltonian
- The adjoint hamiltonian (just to verify that it is, in fact, hermitian)
- The eigenvalues and eigenvectors of the hamiltonian

It also defines some functions to print out into log files each one of those
elements of the simulation.

Some functions may come if/when needed.

---

To test the module, one could use the Jupyter notebook in this repo to see the
quantum simulation or create another Python file and import the `qc_calc_hamiltonian`
module like:

`import qc_calc_hamiltonian as qcch`
